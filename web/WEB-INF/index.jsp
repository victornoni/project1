<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Servlet Hello World</title>
    <style>.error {
        color: red;
    }
    .success {
        color: green;
    }</style>
</head>
<body>
    <div>
       <div>
           <table border="2">
               <tr>
                   <th>Country</th>
                   <th>City</th>
                   <th>Address</th>
               </tr>

               <c:forEach var="adr"  items="${addresses}">
               <tr>
                       <td><c:out value="${adr.country}"></c:out> </td>
                       <td><c:out value="${adr.city}"/> </td>
                       <td><c:out value="${adr.address}"/></td>
               </tr>
               </c:forEach>
           </table>
        </div>

        <div align="left">
            <c:forEach var="phone"  items="${phones}">
            <p> <c:out value="${phone.value}"/><p>
            </c:forEach>
        </div>

        <div>
            <c:forEach var="person"  items="${persons}">
            <p> <c:out value="${person.firstName}"/><p>
            </c:forEach>
        </div>
</div>

</body>
</html>