package connections;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by student on 2/6/2018.
 */

public class Setting {
    private static Connection connection;
    private static org.postgresql.Driver driver1;

    public Setting() throws ClassNotFoundException {

    }

    public static Connection getConnection() throws SQLException, ClassNotFoundException {
        driver1 = new org.postgresql.Driver();
        try {
            Class.forName(driver1.getClass().getName());
            if (connection == null) {
                connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Students", "postgres", "postgres");
            }
        } catch (Exception e) {
        }

        return connection;
    }

    public static void resetAll() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}


