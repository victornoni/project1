package com.practica.controller.dao;

import com.practica.controller.model.Teacher;
import connections.Setting;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/8/2018.
 */
public class TeacherDao {

    private PreparedStatement preparedStatement = null;
    private LibraryAbonamentDao libraryAbonamentDao = new LibraryAbonamentDao();
    private AddressDao addressDao = new AddressDao();

    public TeacherDao() {

    }

    public Teacher getById(Long id) throws SQLException, ClassNotFoundException {
        preparedStatement = Setting.getConnection().prepareStatement("SELECT * FROM teachers JOIN persons on teachers.id = persons.id WHERE id = ?");
        preparedStatement.setLong(1, id);
        ResultSet rs = preparedStatement.executeQuery();
        rs.next();
        Teacher teacher = new Teacher();
        teacher.setId(rs.getLong("id"));
        teacher.setSalary(rs.getDouble("salary"));
        teacher.setFirstName(rs.getString("first_name"));
        teacher.setLastName(rs.getString("last_name"));
        teacher.setBirth(rs.getDate("birth"));
        teacher.setAddress(addressDao.getById(rs.getLong("address_id")));
        teacher.setMail(rs.getString("mail"));
        teacher.setGender(rs.getString("gender"));
        teacher.setPicture(rs.getByte("picture"));
        teacher.setLibraryAbonament(libraryAbonamentDao.getById(rs.getLong("library_abonament_id")));

        return teacher;

    }

    public List<Teacher> getAllTeacher() throws SQLException, ClassNotFoundException {
        List<Teacher> teachers = new ArrayList<Teacher>();
        preparedStatement = Setting.getConnection().prepareStatement("SELECT * FROM teachers JOIN persons on teachers.id = persons.id");
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next());
            Teacher teacher = new Teacher();
            teacher.setId(rs.getLong("id"));
            teacher.setSalary(rs.getDouble("salary"));
            teacher.setFirstName(rs.getString("first_name"));
            teacher.setLastName(rs.getString("last_name"));
            teacher.setBirth(rs.getDate("birth"));
            teacher.setAddress(addressDao.getById(rs.getLong("address_id")));
            teacher.setMail(rs.getString("mail"));
            teacher.setGender(rs.getString("gender"));
            teacher.setLibraryAbonament(libraryAbonamentDao.getById(rs.getLong("library_abonament_id")));
            teachers.add(teacher);

        return teachers;

    }
}
