package com.practica.controller.dao;

import com.practica.controller.model.Discipline;
import com.practica.controller.model.Mark;
import connections.Setting;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/8/2018.
 */
public class MarkDao {
    private PreparedStatement preparedStatement = null;
    private StudentDao studentDao = new StudentDao();
    private DisciplineDao disciplineDao = new DisciplineDao();
    private TeacherDao teacherDao = new TeacherDao();

    public MarkDao(){

    }

    public Mark getById(Long id) throws SQLException, ClassNotFoundException {
        preparedStatement = Setting.getConnection().prepareStatement("SELECT * FROM marks WHERE id = ?");
        preparedStatement.setLong(1, id);
        ResultSet rs = preparedStatement.executeQuery();
        rs.next();
        Mark mark = new Mark();
        mark.setId(rs.getLong("id"));
        mark.setValue(rs.getDouble("value"));
        mark.setStudent(studentDao.getById(rs.getLong("student_id")));
        mark.setDiscipline((Discipline) disciplineDao.getById(rs.getLong("discipline_id")));
        mark.setTeacher(teacherDao.getById(rs.getLong("teacher_id")));
        mark.setCreateDate(rs.getDate("create_date"));

        return mark;
    }

    public List<Mark> getAllMarks() throws SQLException, ClassNotFoundException {
        List<Mark> marks = new ArrayList<Mark>();
        preparedStatement = Setting.getConnection().prepareStatement("SELECT * FROM marks");
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()){
            Mark mark= new Mark();
            mark.setId(rs.getLong("id"));
            mark.setValue(rs.getDouble("value"));
            mark.setStudent(studentDao.getById(rs.getLong("student_id")));
            mark.setDiscipline((Discipline) disciplineDao.getById(rs.getLong("discipline_id")));
            mark.setCreateDate(rs.getDate("create_date"));
            mark.setTeacher(teacherDao.getById(rs.getLong("teacher_id")));
            marks.add(mark);
        }
        return marks;
    }

}
