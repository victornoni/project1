package com.practica.controller.dao;

import com.practica.controller.model.Phone;
import com.practica.controller.model.PhoneType;
import connections.Setting;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/6/2018.
 */
public class PhoneDao {
    private PreparedStatement preparedStatement = null;
    private PhoneTypeDao phoneTypeDao = new PhoneTypeDao();

    public PhoneDao() {
        //default constructor
    }

    public List<Phone> getById(Long id) throws SQLException, ClassNotFoundException {
        List<Phone> phones = new ArrayList<Phone>();
        preparedStatement = Setting.getConnection().prepareStatement("SELECT * from phones JOIN persons_to_phones on phones.id = persons_to_phones.phone_id WHERE person_id = ?");
        preparedStatement.setLong(1, id);
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            Phone phone = new Phone();
            PhoneType phoneType = new PhoneType();
            phone.setId(rs.getLong("id"));
            phone.setValue(rs.getString("value"));
            phone.setPhoneType(phoneTypeDao.getById(rs.getLong("type_id")));
            phones.add(phone);
        }
        return phones;

    }

    public List<Phone> getAllPhone() throws SQLException, ClassNotFoundException {
        List<Phone> phones = new ArrayList<Phone>();
        preparedStatement = Setting.getConnection().prepareStatement("SELECT * from phones");
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            Phone phone = new Phone();
            phone.setId(rs.getLong("id"));
            phone.setValue(rs.getString("value"));
            phone.setPhoneType(phoneTypeDao.getById(rs.getLong("type_id")));
            phones.add(phone);
        }
        return phones;
    }

}
