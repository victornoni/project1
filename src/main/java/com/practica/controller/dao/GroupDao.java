package com.practica.controller.dao;

import com.practica.controller.model.Group;
import connections.Setting;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/6/2018.
 */
public class GroupDao {

    private PreparedStatement preparedStatement = null;

    public GroupDao() {

    }

    public Group getById(Long id) throws SQLException, ClassNotFoundException {
        preparedStatement = Setting.getConnection().prepareStatement("SELECT * from groups WHERE id = ?");
        preparedStatement.setLong(1, id);
        ResultSet rs = preparedStatement.executeQuery();
        rs.next();
        Group group = new Group();
        group.setId(rs.getLong("id"));
        group.setName(rs.getString("name"));

        return group;
    }

    public List<Group> getAllGroup() throws SQLException, ClassNotFoundException {
        List<Group> groups = new ArrayList<Group>();
        preparedStatement = Setting.getConnection().prepareStatement("SELECT * from groups");
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            Group gr = new Group();
            gr.setId(rs.getLong("id"));
            gr.setName(rs.getString("name"));
            groups.add(gr);
        }
        return groups;
    }

    public void deleteById(){

    }

    public void save(){

    }
}
