package com.practica.controller.dao;

import com.practica.controller.model.PhoneType;
import com.sun.org.apache.regexp.internal.RE;
import connections.Setting;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/6/2018.
 */
public class PhoneTypeDao {
    private PreparedStatement preparedStatement = null;

    public PhoneTypeDao() {

    }

    public PhoneType getById(Long id) throws SQLException, ClassNotFoundException {
        preparedStatement = Setting.getConnection().prepareStatement("SELECT * from phone_types WHERE id = ?");
        preparedStatement.setLong(1, id);
        ResultSet rs = preparedStatement.executeQuery();
        rs.next();
        PhoneType phoneType = new PhoneType();
        phoneType.setId(rs.getLong("id"));
        phoneType.setName(rs.getString("name"));

        return phoneType;
    }


    public List<PhoneType> getAllPhoneType() throws SQLException, ClassNotFoundException {
        List<PhoneType> phoneTypes = new ArrayList<PhoneType>();
        preparedStatement = Setting.getConnection().prepareStatement("SELECT * from phone_types");
        ResultSet rs = preparedStatement.executeQuery();
        phoneTypes = new ArrayList<PhoneType>(rs.getFetchSize());
        while (rs.next()) {
            PhoneType pht = new PhoneType();
            pht.setId(rs.getLong("id"));
            pht.setName(rs.getString("name"));
            phoneTypes.add(pht);
        }
        return phoneTypes;
    }

}
