package com.practica.controller.dao;

import com.practica.controller.model.Address;
import connections.Setting;


import javax.servlet.jsp.jstl.sql.SQLExecutionTag;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/6/2018.
 */
public class AddressDao {

    private PreparedStatement preparedStatement = null;

    public AddressDao(){

    }

    public Address getById(Long id) throws SQLException, ClassNotFoundException {
        preparedStatement = Setting.getConnection().prepareStatement("SELECT * from addresses where id = ?");
        preparedStatement.setLong(1, id);
        ResultSet rs = preparedStatement.executeQuery();
        rs.next();
        Address adr = new Address();
        adr.setId(rs.getLong("id"));
        adr.setAddress(rs.getString("address"));
        adr.setCountry(rs.getString("country"));
        adr.setCity(rs.getString("city"));

        return adr;
    }


    public List<Address> getAllAddresses() throws SQLException, ClassNotFoundException {

        List<Address> addresses = new ArrayList<Address>();
        preparedStatement = Setting.getConnection().prepareStatement("SELECT * from addresses");
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            Address adr = new Address();
            adr.setCity(rs.getString("city"));
            adr.setCountry(rs.getString("country"));
            adr.setAddress(rs.getString("address"));
            adr.setId(rs.getLong("id"));
            addresses.add(adr);
        }
        return addresses;
    }

}
