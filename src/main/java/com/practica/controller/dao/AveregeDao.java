package com.practica.controller.dao;

import com.practica.controller.model.Averege;
import connections.Setting;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/7/2018.
 */
public class AveregeDao {

    private PreparedStatement preparedStatement = null;
    private DisciplineDao disciplineDao = new DisciplineDao();

    public AveregeDao() {

    }

    public Averege getById(Long id) throws SQLException, ClassNotFoundException {
        preparedStatement = Setting.getConnection().prepareStatement("Select * from averages");
        preparedStatement.setLong(1, id);
        ResultSet rs = preparedStatement.executeQuery();
        rs.next();
        Averege averege = new Averege();
        averege.setId(rs.getLong("id"));
        averege.setValue(rs.getDouble("value"));
        averege.setDiscipline(disciplineDao.getById(rs.getLong("id")));

        return averege;
    }



    public List<Averege> getAllAvereges() throws SQLException, ClassNotFoundException {

        List<Averege> avereges = new ArrayList<Averege>();
        preparedStatement = Setting.getConnection().prepareStatement("Select * from averages");
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            Averege averege = new Averege();
            averege.setId(rs.getLong("id"));
            averege.setValue(rs.getDouble("value"));
            averege.setDiscipline(disciplineDao.getById(rs.getLong("id")));

            avereges.add(averege);

        }
        return avereges;
    }
}
