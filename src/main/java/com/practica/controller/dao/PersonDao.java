package com.practica.controller.dao;

import com.practica.controller.model.Person;
import connections.Setting;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/7/2018.
 */
public class PersonDao {
    private PreparedStatement preparedStatement = null;
    private AddressDao addressDao = new AddressDao();
    private LibraryAbonamentDao libraryAbonamentDao = new LibraryAbonamentDao();
    private PhoneDao phoneDao = new PhoneDao();

    public  PersonDao(){

    }

    public Person getById(Long id) throws SQLException, ClassNotFoundException {
        preparedStatement = Setting.getConnection().prepareStatement("SELECT * FROM persons WHERE id = ?");
        preparedStatement.setLong(1, id);
        ResultSet rs = preparedStatement.executeQuery();
        rs.next();
        Person person = new Person();
        person.setId(rs.getLong("id"));
        person.setFirstName(rs.getString("first_name"));
        person.setLastName(rs.getString("last_name"));
        person.setGender(rs.getString("gender"));
        person.setBirth(rs.getDate("date_of_birth"));
        person.setAddress(addressDao.getById(rs.getLong("address_id")));
        person.setMail(rs.getString("mail"));
        person.setLibraryAbonament(libraryAbonamentDao.getById(rs.getLong("library_abonament_id")));
        person.setPicture(rs.getByte("picture"));
        person.setPhones(phoneDao.getById(rs.getLong("id")));

        return person;

    }

    public List<Person> getAllPeople() throws SQLException, ClassNotFoundException{
        List<Person> people = new ArrayList<Person>();
        preparedStatement = Setting.getConnection().prepareStatement("SELECT * FROM persons");
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()){
            Person person = new Person();
            person.setId(rs.getLong("id"));
            person.setFirstName(rs.getString("first_name"));
            person.setLastName(rs.getString("last_name"));
            person.setGender(rs.getString("gender"));
            person.setBirth(rs.getDate("date_of_birth"));
            person.setAddress(addressDao.getById(rs.getLong("address_id")));
            person.setMail(rs.getString("mail"));
            person.setLibraryAbonament(libraryAbonamentDao.getById(rs.getLong("Library_abonament_id")));
            person.setPicture(rs.getByte("picture"));
            person.setPhones(phoneDao.getById(rs.getLong("id")));
            people.add(person);
        }
        return people;
    }
}
