package com.practica.controller.dao;

import com.practica.controller.model.Discipline;
import connections.Setting;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/7/2018.
 */
public class DisciplineDao {
    private PreparedStatement preparedStatement = null;
    private TeacherDao teacherDao = new TeacherDao();

    public DisciplineDao() {

    }

    public List<Discipline> getById(Long id) throws SQLException, ClassNotFoundException {
        List<Discipline> disciplines = new ArrayList<Discipline>();
        preparedStatement = Setting.getConnection().prepareStatement("Select * from disciplines WHERE id = ?");
        preparedStatement.setLong(1, id);
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            Discipline discipline = new Discipline();
            discipline.setId(rs.getLong("id"));
            discipline.setTitle(rs.getString("title"));
            discipline.setTeacher(teacherDao.getById(rs.getLong("teacher_id")));
            disciplines.add(discipline);

        }
        return disciplines;
    }

    public List<Discipline> getAllDisciplines() throws SQLException, ClassNotFoundException {
        List<Discipline> disciplines = new ArrayList<Discipline>();
        preparedStatement = Setting.getConnection().prepareStatement("Select * from disciplines");
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            Discipline discipline = new Discipline();
            discipline.setId(rs.getLong("id"));
            discipline.setTitle(rs.getString("title"));
            discipline.setTeacher(teacherDao.getById(rs.getLong("teacher_id")));

            disciplines.add(discipline);

        }
        return disciplines;
    }
}
