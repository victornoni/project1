package com.practica.controller.dao;

import com.practica.controller.model.Student;
import connections.Setting;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/8/2018.
 */
public class StudentDao {
    private PreparedStatement preparedStatement = null;
    private GroupDao groupDao = new GroupDao();
    private LibraryAbonamentDao libraryAbonamentDao = new LibraryAbonamentDao();
    private AddressDao addressDao = new AddressDao();


    public StudentDao() {

    }

    public Student getById(Long id) throws SQLException, ClassNotFoundException {
        preparedStatement = Setting.getConnection().prepareStatement("SELECT * FROM Students WHERE id = ?");
        preparedStatement.setLong(1, id);
        ResultSet rs = preparedStatement.executeQuery();
        rs.next();
        Student student = new Student();
        student.setId(rs.getLong("id"));
        student.setFirstName(rs.getString("first_name"));
        student.setLastName(rs.getString("last_name"));
        student.setGroup(groupDao.getById(rs.getLong("group_id")));
        student.setGender(rs.getString("gender"));
        student.setBirth(rs.getDate("date_of_birth"));
        student.setAddress(addressDao.getById(rs.getLong("address")));
        student.setMail(rs.getString("mail"));
        student.setLibraryAbonament(libraryAbonamentDao.getById(rs.getLong("library_abonament_id")));
        student.setPicture(rs.getByte("picture"));

        return student;

    }


    public List<Student> getAllStudents() throws SQLException, ClassNotFoundException {
        List<Student> students = new ArrayList<Student>();
        preparedStatement = Setting.getConnection().prepareStatement("SELECT * FROM Students");
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()){
            Student student = new Student();
            student.setId(rs.getLong("id"));
            student.setFirstName(rs.getString("first_name"));
            student.setLastName(rs.getString("last_name"));
            student.setGroup(groupDao.getById(rs.getLong("group_id")));
            student.setGender(rs.getString("gender"));
            student.setBirth(rs.getDate("date_of_birth"));
            student.setAddress(addressDao.getById(rs.getLong("address")));
            student.setMail(rs.getString("mail"));
            student.setLibraryAbonament(libraryAbonamentDao.getById(rs.getLong("library_abonament_id")));
            student.setPicture(rs.getByte("picture"));
            students.add(student);
        }
        return students;

    }


}
