package com.practica.controller.dao;

import com.practica.controller.model.LibraryAbonament;
import connections.Setting;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/6/2018.
 */
public class LibraryAbonamentDao {
    private PreparedStatement preparedStatement = null;

    public LibraryAbonamentDao() {

    }

    public LibraryAbonament getById(Long id) throws SQLException, ClassNotFoundException {
        preparedStatement = Setting.getConnection().prepareStatement("SELECT * FROM library_abonaments WHERE id = ?");
        preparedStatement.setLong(1, id);
        ResultSet rs = preparedStatement.executeQuery();
        rs.next();
        LibraryAbonament libraryAbonament = new LibraryAbonament();
        libraryAbonament.setId(rs.getLong("id"));
        libraryAbonament.setStatus(rs.getString("status"));
        libraryAbonament.setStartDate(rs.getDate("start_date"));
        libraryAbonament.setEndDate(rs.getDate("end_date"));

        return libraryAbonament;
    }


    public List<LibraryAbonament> getAllLibraryAbonament() throws SQLException, ClassNotFoundException {
        List<LibraryAbonament> libraryAbonaments = new ArrayList<LibraryAbonament>();
        preparedStatement = Setting.getConnection().prepareStatement("SELECT * from library_abonaments");
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            LibraryAbonament libraryAbonament = new LibraryAbonament();
            libraryAbonament.setId(rs.getLong("id"));
            libraryAbonament.setStatus(rs.getString("status"));
            libraryAbonament.setStartDate(rs.getDate("start_date"));
            libraryAbonament.setEndDate(rs.getDate("end_date"));
            libraryAbonaments.add(libraryAbonament);

        }
        return libraryAbonaments;
    }
}
