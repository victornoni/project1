package com.practica.controller.Servlets;

import com.practica.controller.dao.AddressDao;
import com.practica.controller.dao.DisciplineDao;
import com.practica.controller.dao.PersonDao;
import com.practica.controller.dao.PhoneDao;
import com.practica.controller.model.Address;
import com.practica.controller.model.Discipline;
import com.practica.controller.model.Phone;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/5/2018.
 */
@WebServlet("/index")
public class HomeServlet extends HttpServlet {
    private AddressDao addressDao = new AddressDao();
    private PhoneDao phoneDao = new PhoneDao();
    private PersonDao personDao = new PersonDao();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        try {
            request.setAttribute("addresses", addressDao.getAllAddresses());
            request.setAttribute("phones", phoneDao.getAllPhone());
            request.setAttribute("persons", personDao.getAllPeople());
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        //request.setAttribute("addresses", addresses);

        request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
    }
}