package com.practica.controller.model;

/**
 * Created by student on 2/6/2018.
 */
public class DisciplineToStudent {
    private Student student;
    private Discipline discipline;

    public Student getStudent(){
        return student;
    }
    public void setStudent(Student student){
        this.student = student;
    }

    public Discipline getDiscipline(){
        return discipline;
    }
    public void setDiscipline(Discipline discipline){
        this.discipline = discipline;
    }

}
