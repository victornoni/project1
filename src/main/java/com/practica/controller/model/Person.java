package com.practica.controller.model;

import com.sun.org.apache.bcel.internal.generic.LUSHR;

import java.sql.Date;
import java.util.List;

/**
 * Created by student on 2/6/2018.
 */
public class Person {
    private Long id;
    private String firstName;
    private String lastName;
    private Date birth;
    private String gender;
    private Byte picture;
    private String mail;
    private Address address;
    private LibraryAbonament libraryAbonament;
    private List<Phone> phones;

    public Long getId(){
        return id;
    }
    public void setId(Long id){
        this.id = id;
    }

    public String getFirstName(){
        return firstName;
    }
    public void setFirstName(String firstName){
        this.firstName = firstName;
    }

    public String getLastName(){
        return lastName;
    }
    public void setLastName(String lastName){
        this.lastName = lastName;
    }

    public Date getBirth(){
        return birth;
    }
    public void setBirth(Date birth){
        this.birth = birth;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public String getGender(){
        return gender;
    }
    public void setGender(String gender){
        this.gender = gender;
    }

    public Byte getPicture(){
        return picture;
    }
    public void setPicture(Byte picture){
        this.picture = picture;
    }

    public String getMail(){
        return mail;
    }
    public void setMail(String mail){
        this.mail = mail;
    }

    public Address getAddress(){
        return address;
    }
    public void setAddress(Address address){
        this.address = address;
    }

    public LibraryAbonament getLibraryAbonament(){
        return libraryAbonament;
    }
    public void setLibraryAbonament(LibraryAbonament libraryAbonament){
        this.libraryAbonament = libraryAbonament;
    }

}
