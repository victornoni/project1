package com.practica.controller.model;

import java.util.List;

/**
 * Created by student on 2/6/2018.
 */
public class Averege {
    private Student student;
    private List<Discipline> disciplines;
    private Long id;
    private Double value;

    public Long getId(){
        return id;
    }
    public void setId(Long id){
        this.id = id;
    }

    public Student getStudent(){
        return student;
    }
    public void setStudent(Student student){
        this.student = student;
    }

    public List<Discipline> getDiscipline(){
        return disciplines;
    }
    public void setDiscipline(List<Discipline> disciplines){
        this.disciplines = disciplines;
    }

    public Double getValue(){
        return value;
    }
    public void setValue(Double value){
        this.value = value;
    }

}
