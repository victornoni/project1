package com.practica.controller.model;

/**
 * Created by student on 2/6/2018.
 */
public class Teacher extends Person {
    private Long id;
    private Double salary;

    public Long getId(){
        return id;
    }
    public void setId(Long id){
        this.id = id;
    }

    public Double getSalary(){
        return salary;
    }
    public void setSalary(Double salary){
        this.salary = salary;
    }

}
