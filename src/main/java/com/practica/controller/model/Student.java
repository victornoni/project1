package com.practica.controller.model;

/**
 * Created by student on 2/6/2018.
 */
public class Student extends Person {
    private Group group;
    private Long id;

    public Group getGroup(){
        return group;
    }
    public void setGroup(Group group){
        this.group = group;
    }

    public Long getId(){
        return id;
    }
    public void setId(Long id){
        this.id = id;
    }

}
