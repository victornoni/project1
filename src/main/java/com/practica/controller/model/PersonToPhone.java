package com.practica.controller.model;

/**
 * Created by student on 2/6/2018.
 */
public class PersonToPhone {
    private Person person;
    private Phone phone;


    public Person getPerson(){
        return person;
    }
    public void setPerson(Person person){
        this.person = person;
    }

    public Phone getPhone(){
        return phone;
    }
    public void setPhone(Phone phone){
        this.phone = phone;
    }
}
