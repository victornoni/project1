package com.practica.controller.model;


import java.sql.Date;

/**
 * Created by student on 2/6/2018.
 */
public class Mark {
    private Long id;
    private Double value;
    private Date createDate;
    private Student student;
    private Discipline discipline;
    private Teacher teacher;


    public Long getId(){
        return id;
    }
    public void setId(Long id){
        this.id = id;
    }

    public Double getValue(){
        return value;
    }
    public void setValue(Double value){
        this.value = value;
    }

    public Date getCreateDate(){
        return createDate;
    }
    public void setCreateDate(Date createDate){
        this.createDate = createDate;
    }

    public Student getStudent(){
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Discipline getDiscipline(){
        return discipline;
    }
    public void setDiscipline(Discipline discipline){
        this.discipline = discipline;
    }

    public Teacher getTeacher(){
        return teacher;
    }
    public void setTeacher(Teacher teacher){
        this.teacher = teacher;
    }
}
